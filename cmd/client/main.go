package main

import (
	"bufio"
	"context"
	"fmt"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/client"
	pb "gitlab.com/Sonichka1311/soa-hw4/pkg/proto"
	"google.golang.org/grpc"
	"log"
	"os"
	"strings"
	"time"
)

func main() {
	// create the connection
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	conn, err := grpc.Dial(":9000", opts...)
	if err != nil {
		log.Fatalf(err.Error())
	}
	defer conn.Close()

	// create the client
	gameClient := client.NewClient(pb.NewMafiaClient(conn))

	reader := bufio.NewReader(os.Stdin)
	gameClient.SetReader(reader)

	// read username
	fmt.Println("To start the game, please, provide your name (should be unique).")
	name, _ := reader.ReadString('\n')
	for {
		registerStatus, err := gameClient.RegisterUser(context.Background(), &pb.User{Username: name})
		if err != nil {
			log.Fatalln("Failed to connect the server. Please, try later.")
		}
		if registerStatus.Ok {
			break
		}
		fmt.Printf("Error: %s. Please try again.\n", registerStatus.Error)
		name, _ = reader.ReadString('\n')
		name = strings.TrimSuffix(name, "\n")
	}
	fmt.Printf("Welcome, @%s\n", name)

	//for {
		// try to start the game
		log.Println("Starting new game...")
		stream, err := gameClient.JoinGame(context.Background())
		if err != nil {
			for i := 0; i < 10; i++ {
				log.Println("Failed to start the game. Retrying in 10s...")
				time.Sleep(10 * time.Second)
				stream, err = gameClient.JoinGame(context.Background())
				if err == nil {
					break
				}
			}
			if err != nil {
				log.Fatalln("Failed to start the game.")
			}
		}
		gameClient.SetStream(stream)
		stream.Send(&pb.Command{
			Username: name,
			Command:  "/join",
		})

		wait := make(chan struct{})
		statuses := make(chan *pb.Status)

		go gameClient.HandleServerMessages(name, statuses, wait)

		gameClient.HandleUserMessages(statuses)

		stream.CloseSend()
		<-wait
	//
	//	fmt.Println("The game session is over. Would you like to start the new session?\nSend yes to start the new session or any other word to exit.")
	//	decision, _ := reader.ReadString('\n')
	//	decision = strings.TrimSpace(decision)
	//	if decision != "yes" {
	//		log.Printf(`The decision was %s` + "\n", decision)
	//		break
	//	}
	//}
	defer gameClient.DeleteUser(context.Background(), &pb.User{Username: name})
}