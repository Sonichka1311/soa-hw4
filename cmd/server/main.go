package main

import (
	"flag"
	pb "gitlab.com/Sonichka1311/soa-hw4/pkg/proto"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/server"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	pb.RegisterMafiaServer(grpcServer, server.NewServer())
	grpcServer.Serve(lis)
}