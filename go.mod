module gitlab.com/Sonichka1311/soa-hw4

go 1.15

require (
	github.com/golang/protobuf v1.4.3
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
)
