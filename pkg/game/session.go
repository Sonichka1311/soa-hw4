package game

import (
	"fmt"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/chat"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/constants"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/user"
	"log"
	"math/rand"
	"time"
)

type Session struct {
	Users         user.GameUsers
	Status        string
	Chat          chat.Chat
	userToRole    map[user.User]string
	alive         int
	mafia         int
	whoIsMafia    string
	votesForUsers map[user.User]int
	votesByUsers  map[user.User]struct{}
	endDay        map[user.User]struct{}
	copHasAnswer  bool
}

type Sessions []*Session

func StartNewSession(users user.GameUsers) *Session {
	session := Session{
		Users:      users,
		Status:     constants.StatusStarted,
		Chat:       nil,
		userToRole: map[user.User]string{},
		alive:      len(users),
		mafia:      1,
		endDay:     map[user.User]struct{}{},
	}

	roles := []string{constants.RoleCivilian, constants.RoleCivilian, constants.RoleCop, constants.RoleMafia}
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(roles), func(i, j int) { roles[i], roles[j] = roles[j], roles[i] })

	for idx, usr := range users {
		session.userToRole[usr] = roles[idx]
		log.Printf("user @%s, role %s", usr, roles[idx])
	}
	return &session
}

func (s *Session) GetRoleForUser(username string) string {
	if role, exists := s.userToRole[user.User(username)]; exists {
		return role
	}
	return constants.RoleDead
}

func (s *Session) RemoveUser(usr user.User) {
	s.Users.RemoveUser(usr)
	s.Chat.NewMessage("admin", fmt.Sprintf("user @%s left the game", usr))
	s.killUser(usr)
	delete(s.userToRole, usr)
}

func (s *Session) killUser(usr user.User) {
	if s.userToRole[usr] != constants.RoleDead {
		s.alive--
	}
	if s.userToRole[usr] == constants.RoleMafia {
		s.mafia--
	}
	if s.alive-s.mafia == s.mafia {
		s.Status = constants.StatusEnded
		s.whoIsMafia = constants.RoleMafia
		s.Chat.NewMessage("admin", "Game ended. Mafia won.")
	} else if s.mafia == 0 {
		s.Status = constants.StatusEnded
		s.whoIsMafia = constants.RoleCivilian + "s"
		s.Chat.NewMessage("admin", "Game ended. Civilians won.")
	}
	s.userToRole[usr] = constants.RoleDead
}

func (s *Session) WhoIsMafia() string {
	return s.whoIsMafia
}

func (s *Session) IsMafia(username string) {
	s.copHasAnswer = true
	answer := " not"
	if s.GetRoleForUser(username) == constants.RoleMafia {
		s.whoIsMafia = username
		answer = ""
	}
	s.Chat.NewMessage("admin->cop", fmt.Sprintf("User @%s is%s a mafia", username, answer))
	log.Printf("Cop receive an answer.")
	if len(s.votesByUsers) == s.mafia && s.copHasAnswer {
		s.endPeriod(constants.StatusDay)
	}
}

func (s *Session) endPeriod(nextPeriod string) {
	kill := user.User("")
	votes := 0
	for voted, count := range s.votesForUsers {
		if count > votes {
			kill = voted
			votes = count
		}
	}
	if votes != 0 {
		s.Chat.NewMessage("admin", fmt.Sprintf("user @%s has been killed", kill))
		s.killUser(kill)
	}
	if s.Status != constants.StatusEnded {
		s.Status = nextPeriod
	}

	s.votesForUsers = map[user.User]int{}
	s.votesByUsers = map[user.User]struct{}{}
	s.endDay = map[user.User]struct{}{}
	s.copHasAnswer = false
}

func (s *Session) VoteFor(usr user.User, to string) {
	if s.GetRoleForUser(to) == constants.RoleDead {
		return
	}
	if _, exists := s.votesByUsers[usr]; exists {
		return
	}
	if s.GetRoleForUser(to) == constants.RoleDead {
		return
	}
	s.votesForUsers[user.User(to)]++
	s.votesByUsers[usr] = struct{}{}
	s.Chat.NewMessage("admin", fmt.Sprintf("user @%s voted to kill @%s", usr, to))
	if len(s.votesByUsers) == s.alive {
		log.Println("Users made a choice.")
		s.endPeriod(constants.StatusNight)
	} else {
		log.Printf("There are %d users voted at this moment.\n", len(s.votesByUsers))
	}
}

func (s *Session) VoteForByMafia(usr user.User, to string) {
	if s.GetRoleForUser(to) == constants.RoleDead {
		return
	}
	if _, exists := s.votesByUsers[usr]; exists {
		return
	}
	if s.GetRoleForUser(to) == constants.RoleDead {
		return
	}
	s.votesForUsers[user.User(to)]++
	s.votesByUsers[usr] = struct{}{}
	s.Chat.NewMessage("admin->mafia", fmt.Sprintf("user @%s voted to kill @%s", usr, to))
	if len(s.votesByUsers) == s.mafia {
		log.Println("Mafia made a choice.")
	} else {
		log.Printf("There are %d mafia voted at this moment.\n", len(s.votesByUsers))
	}
	if len(s.votesByUsers) == s.mafia && s.copHasAnswer {
		s.endPeriod(constants.StatusDay)
	}
}

func (s *Session) EndDay(usr user.User) {
	if _, exists := s.endDay[usr]; exists {
		return
	}
	s.endDay[usr] = struct{}{}
	s.Chat.NewMessage("admin", fmt.Sprintf("user @%s voted to end the day", usr))
	if len(s.endDay) == s.alive {
		s.endPeriod(constants.StatusNight)
	}
}

func (s *Session) PublishMafia() {
	s.Chat.NewMessage("admin", fmt.Sprintf("cop had calculated the mafia. Is is @%s", s.whoIsMafia))
	s.whoIsMafia = ""
}

func (s *Session) HasUser(username string) bool {
	usr := user.User(username)
	for _, u := range s.Users {
		if u == usr {
			return true
		}
	}
	return false
}
