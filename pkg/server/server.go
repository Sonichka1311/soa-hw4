package server

import (
	"context"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/constants"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/game"
	pb "gitlab.com/Sonichka1311/soa-hw4/pkg/proto"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/user"
	"io"
	"log"
	"strings"
	"sync"
	"time"
)

type Server struct {
	pb.UnimplementedMafiaServer
	users user.Users
	games game.Sessions
	waitingList user.GameUsers
	userToSession map[user.User]*game.Session
	mutex sync.Mutex
}

func NewServer() *Server {
	return &Server{
		users: user.Users{},
		games: game.Sessions{&game.Session{Status: constants.StatusWaiting}},
		waitingList: make(user.GameUsers, 0),
		userToSession: make(map[user.User]*game.Session),
		mutex: sync.Mutex{},
	}
}

func (s *Server) RegisterUser(_ context.Context, user *pb.User) (*pb.RegisterStatus, error) {
	res := pb.RegisterStatus{
		Ok: true,
	}

	err := s.users.AddUser(strings.TrimSuffix(user.Username, "\n"))
	if err != nil {
		res.Ok = false
		res.Error = err.Error()
	} else {
		log.Printf("Added user @%s", user.Username)
	}

	return &res, nil
}

func (s *Server) DeleteUser(_ context.Context, usr *pb.User) (*pb.User, error) {
	delete(s.userToSession, user.User(usr.Username))
	return usr, s.users.RemoveUser(usr.Username)
}

func (s *Server) JoinGame(stream pb.Mafia_JoinGameServer) error {
	cmd, _ := stream.Recv()
	username := strings.TrimSuffix(cmd.Username, "\n")

	log.Printf("User @%s is waiting for new session.", username)
	session := s.waitUsersAndStartTheSession(username, stream)
	log.Println("New session has been started.")

	end := false
	go func() {
		for {
			in, err := stream.Recv()
			if err == io.EOF {
				log.Printf("Received empty message from user @%s", username)
				return
			}
			if err != nil {
				log.Printf("Received errored message (err: %s) from user @%s", err.Error(), username)
				return
			}
			in.Command = strings.TrimSuffix(in.Command, "\n")
			log.Printf("Received message `%s` from user @%s", in.Command, username)
			status := s.handleUserMessage(username, in)
			if !status {
				end = true
				break
			}
		}
	}()

	equal := func(a, b *pb.Status) bool {
		if a.GameStatus != b.GameStatus || a.Role != b.Role || a.Name != b.Name || a.Mafia != b.Mafia {
			log.Println("return 1")
			return false
		}

		if len(a.Messages) * len(b.Messages) == 0 && len(a.Messages) != len(b.Messages) {
			log.Println("return 2")
			return false
		}

		la := len(a.Messages)
		if len(a.Messages) > 10 {
			la = 10
		}

		lb := len(b.Messages)
		if len(b.Messages) > 10 {
			lb = 10
		}

		a.Messages = a.Messages[len(a.Messages) - la:len(a.Messages)]
		b.Messages = b.Messages[len(b.Messages) - lb:len(b.Messages)]

		if len(a.Messages) != len(b.Messages) {
			log.Println("return 2.5")
			return false
		}

		for idx := range a.Messages {
			if a.Messages[idx] != b.Messages[idx] {
				log.Println("return 3")
				return false
			}
		}

		if len(a.AllUsers) != len(b.AllUsers) {
			log.Println("return 4")
			return false
		}

		return true
	}

	status := &pb.Status{
		GameStatus: constants.StatusStarted,
		Messages:   nil,
		AllUsers:   session.Users.GetList(),
		Name:       username,
		Role:       session.GetRoleForUser(username),
		Mafia:      "",
	}
	for {
		newStatus := s.getCurrentStatus(username)
		if !equal(status, newStatus) {
			log.Printf("Current game status `%s` for user @%s", newStatus.GameStatus, username)
			if err := stream.Send(newStatus); err != nil {
				return err
			}
			status = newStatus
		}
		if end {
			if err := stream.Send(&pb.Status{GameStatus: constants.StatusExited}); err != nil {
				return err
			}
			return nil
		}
		time.Sleep(100 * time.Millisecond)
	}
}
