package server

import (
	"gitlab.com/Sonichka1311/soa-hw4/pkg/constants"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/game"
	pb "gitlab.com/Sonichka1311/soa-hw4/pkg/proto"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/user"
	"log"
	"strings"
	"time"
)

func (s *Server) waitUsersAndStartTheSession(username string, stream pb.Mafia_JoinGameServer) *game.Session {
	var session = s.games[0] // waiting session
	s.userToSession[user.User(username)] = s.games[0]
	s.mutex.Lock()
	s.waitingList.AddUser(username)
	// check is this user initiate the session
	if len(s.waitingList) == constants.UsersCount {
		session = s.startNewSession()
	}
	count := 0
	s.mutex.Unlock()
	if session != s.games[0] {
		users := make([]string, len(session.Users))
		for idx, usr := range session.Users {
			users[idx] = string(usr)
		}
		stream.Send(&pb.Status{
			GameStatus: session.Status,
			AllUsers:   users,
			Name:       username,
			Role:       session.GetRoleForUser(username),
		})
		return session
	}

	for {
		s.mutex.Lock()
		// check if user enter the session
		if ses, exists := s.userToSession[user.User(username)]; exists {
			session = ses
		}
		if len(s.waitingList) != count && session == s.games[0] {
			log.Printf("There are %d users in waiting list", len(s.waitingList))
			stream.Send(&pb.Status{
				GameStatus: session.Status,
				AllUsers:   s.waitingList.GetList(),
				Name:       username,
				Role:       constants.RoleWaiter,
			})
			count = len(s.waitingList)
		}
		s.mutex.Unlock()
		if session != s.games[0] {
			users := make([]string, len(session.Users))
			for idx, usr := range session.Users {
				users[idx] = string(usr)
			}
			stream.Send(&pb.Status{
				GameStatus: session.Status,
				AllUsers:   users,
				Name:       username,
				Role:       session.GetRoleForUser(username),
			})
			return session
		}
		time.Sleep(10 * time.Millisecond)
	}
}

func (s *Server) startNewSession() *game.Session {
	session := game.StartNewSession(s.waitingList)
	for _, wUser := range s.waitingList {
		if s.userToSession == nil {
			log.Fatalf("map userToSession is nil")
		}
		s.userToSession[wUser] = session
	}
	s.waitingList = nil
	s.games = append(s.games, session)
	return session
}

func (s *Server) handleUserMessage(username string, in *pb.Command) bool {
	usr := user.User(username)
	session := s.userToSession[usr]
	role := session.GetRoleForUser(username)
	isCommand := strings.HasPrefix(in.Command, "/")
	switch session.Status {
	case constants.StatusWaiting:
		if in.Command == "/exit" {
			s.mutex.Lock()
			s.waitingList.RemoveUser(usr)
			session.RemoveUser(usr)
			delete(s.userToSession, usr)
			s.mutex.Unlock()
			return false // do not continue
		} // else do nothing
	case constants.StatusStarted:
		if in.Command == "/exit" {
			s.mutex.Lock()
			session.RemoveUser(usr)
			delete(s.userToSession, usr)
			s.mutex.Unlock()
			return false // do not continue
		}
		s.mutex.Lock()
		if !isCommand {
			session.Chat.NewMessage(usr, in.Command)
		} else if strings.HasPrefix(in.Command, "/end_day") {
			session.EndDay(usr)
		}
		s.mutex.Unlock()
	case constants.StatusDay, constants.StatusNight:
		if role == constants.RoleDead {
			if in.Command == "/exit" {
				s.mutex.Lock()
				session.Users.RemoveUser(usr)
				delete(s.userToSession, usr)
				s.mutex.Unlock()
				return false // do not continue
			}
			return true
		}
		if session.Status == constants.StatusDay {
			if !isCommand {
				s.mutex.Lock()
				session.Chat.NewMessage(usr, in.Command)
				s.mutex.Unlock()
			} else {
				// handle commands
				s.mutex.Lock()
				if strings.HasPrefix(in.Command, "/kill") && strings.Contains(in.Command, "@") {
					session.VoteFor(usr, strings.Split(in.Command, "@")[1])
				} else if strings.HasPrefix(in.Command, "/end_day") {
					session.EndDay(usr)
				} else if strings.HasPrefix(in.Command, "/publish") && role == constants.RoleCop {
					session.PublishMafia()
				}
				s.mutex.Unlock()
			}
		} else if session.Status == constants.StatusNight && (role == constants.RoleMafia || role == constants.RoleCop) {
			if !isCommand {
				s.mutex.Lock()
				to := "mafia"
				if role == constants.RoleCop {
					to = "cop"
				}
				sender := string(usr) + "->" + to
				session.Chat.NewMessage(user.User(sender), in.Command)
				s.mutex.Unlock()
			} else  {
				// handle commands
				s.mutex.Lock()
				if strings.Contains(in.Command, "@") {
					to := strings.Split(in.Command, "@")[1]
					if session.HasUser(to) {
						if strings.HasPrefix(in.Command, "/kill") && role == constants.RoleMafia {
							session.VoteForByMafia(usr, to)
						} else if strings.HasPrefix(in.Command, "/check") && role == constants.RoleCop {
							session.IsMafia(to)
						}
					}
				}
				s.mutex.Unlock()
			}
		}
	case constants.StatusEnded:
		//if in.Command == "/exit" {
		//	s.mutex.Lock()
		//	session.Users.RemoveUser(usr)
		//	delete(s.userToSession, usr)
		//	s.mutex.Unlock()
		//	return false // do not continue
		//}
		return false
	}
	//if session.Status == constants.StatusEnded {
	//	delete(s.userToSession, usr)
	//	return false
	//}
	return true
}

func (s *Server) getCurrentStatus(username string) *pb.Status {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	session := s.userToSession[user.User(username)]
	return &pb.Status{
		GameStatus: session.Status,
		Messages:   session.Chat.GetList(session.GetRoleForUser(username)),
		AllUsers:   session.Users.GetList(),
		Name:       username,
		Role:       session.GetRoleForUser(username),
		Mafia:      session.WhoIsMafia(),
	}
}
