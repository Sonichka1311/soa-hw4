package constants

const (
	StatusWaiting = "waiting"
	StatusStarted = "started"
	StatusDay     = "going-day"
	StatusNight   = "going-night"
	StatusEnded   = "ended"
	StatusExited  = "exited"
)
