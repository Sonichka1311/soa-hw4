package constants

const (
	RoleWaiter   = "waiter"
	RoleCivilian = "civilian"
	RoleMafia    = "mafia"
	RoleCop      = "cop"
	RoleDead     = "dead"
)
