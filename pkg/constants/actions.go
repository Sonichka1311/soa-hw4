package constants

const (
	ActionKill         = "send /kill @username to vote for user @username"
	ActionEndDay       = "send /end_day to end the current day"
	ActionPublishMafia = "send /publish_mafia to publish mafia you have calculated last night"
	ActionCheckUser    = "send /check @username to check user @username"
	ActionSendMessage  = "send any message"
)
