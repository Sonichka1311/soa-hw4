package chat

import (
	"fmt"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/constants"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/user"
	"log"
	"strings"
)

type Message struct {
	Sender user.User
	Msg    string
}

type Chat []*Message

func (c Chat) GetList(role string) []string {
	list := make([]string, 0, len(c))
	getMessage := func(sender, msg string) string {
		return fmt.Sprintf("%s: %s\n", sender, msg)
	}
	for _, msg := range c {
		if strings.Contains(string(msg.Sender), "->") {
			users := strings.Split(string(msg.Sender), "->")
			from := users[0]
			to := users[1]
			if to == role || role == constants.RoleDead {
				list = append(list, getMessage(from, msg.Msg))
			}
		} else {
			list = append(list, getMessage(string(msg.Sender), msg.Msg))
		}
	}
	return list
}

func (c *Chat) NewMessage(sender user.User, msg string) {
	log.Printf("Send message from @%s: %s", sender, msg)
	*c = append(*c, &Message{
		Sender: sender,
		Msg:    msg,
	})
}
