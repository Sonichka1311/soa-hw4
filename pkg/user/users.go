package user

import (
	"fmt"
	"log"
)

type User string

type Users map[User]struct{}

func (u Users) AddUser(username string) error {
	user := User(username)
	if _, exists := u[user]; exists {
		log.Printf("User @%s already exists", username)
		return fmt.Errorf("user with username %s already exists", username)
	}
	u[user] = struct{}{}
	return nil
}

func (u Users) RemoveUser(username string) error {
	user := User(username)
	if _, exists := u[user]; exists {
		delete(u, user)
	}
	return nil
}

type GameUsers []User

func (gu *GameUsers) AddUser(username string) {
	*gu = append(*gu, User(username))
}

func (gu GameUsers) GetList() []string {
	list := make([]string, len(gu))
	for idx, user := range gu {
		list[idx] = string(user)
	}
	return list
}

func (gu *GameUsers) RemoveUser(user User) {
	index := -1
	for idx, usr := range *gu {
		if usr == user {
			index = idx
			break
		}
	}
	if index != -1 {
		if index == len(*gu) - 1 {
			*gu = (*gu)[:index]
		} else if index == 0 {
			*gu = (*gu)[1:]
		} else {
			*gu = append((*gu)[:index], (*gu)[index + 1:]...)
		}
	}
}
