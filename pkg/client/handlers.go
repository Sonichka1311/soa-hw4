package client

import (
	"gitlab.com/Sonichka1311/soa-hw4/pkg/constants"
	pb "gitlab.com/Sonichka1311/soa-hw4/pkg/proto"
	"io"
	"log"
	"time"
)

func (c *Client) HandleServerMessages(username string, statuses chan *pb.Status, wait chan struct{}) {
	for {
		in, err := c.stream.Recv()
		if err == io.EOF {
			// read done
			close(wait)
			return
		}
		if err != nil {
			log.Fatalf("Failed to receive message from a server: %v", err)
		}
		c.replyUser(username, in)
		if in.GameStatus == constants.StatusEnded {
			//log.Println("game ended, channel closed")
			close(wait)
			close(statuses)
			return
		}
		statuses <- in
	}
}

func (c *Client) HandleUserMessages(statuses chan *pb.Status) {
	commands := make(chan string)
	lastStatus := &pb.Status{
		Role: constants.RoleCivilian,
		GameStatus: constants.StatusWaiting,

	}
	go c.getFromClient(commands)
	for {
		select {
		case status, ok := <- statuses:
			//log.Println(status, ok)
			if !ok {
				return
			}
			lastStatus = status
		default:
		}

		tag:
		select {
		case cmd := <- commands:
			if lastStatus.GameStatus == constants.StatusEnded || lastStatus.GameStatus == constants.StatusExited {
				break
			}

			if lastStatus.GameStatus == constants.StatusWaiting || lastStatus.Role == constants.RoleDead {
				goto tag
			}

			if lastStatus.GameStatus == constants.StatusNight && lastStatus.Role == constants.RoleCivilian {
				goto tag
			}

			if err := c.stream.Send(&pb.Command{Command: cmd}); err != nil {
				log.Fatalf("Failed to send a command: %v", err)
			}
		default:
		}
		time.Sleep(100 * time.Millisecond)
	}
}