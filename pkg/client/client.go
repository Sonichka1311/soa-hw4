package client

import (
	"bufio"
	pb "gitlab.com/Sonichka1311/soa-hw4/pkg/proto"
)

type Client struct {
	pb.MafiaClient
	stream pb.Mafia_JoinGameClient
	reader *bufio.Reader
}

func NewClient(client pb.MafiaClient) *Client {
	return &Client{
		MafiaClient: client,
	}
}

func (c *Client) SetStream(stream pb.Mafia_JoinGameClient) {
	c.stream = stream
}

func (c *Client) SetReader(reader *bufio.Reader) {
	c.reader = reader
}