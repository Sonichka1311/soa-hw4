package client

import (
	"fmt"
	"gitlab.com/Sonichka1311/soa-hw4/pkg/constants"
	pb "gitlab.com/Sonichka1311/soa-hw4/pkg/proto"
	"strings"
	"time"
)

const (
	 gameResponse = "Your name: @%s\n" +
	 	"Your role: %s\n" +
		"---------------------------------\nUsers:\n%s\n" +
		"---------------------------------\nChat:\n%s\n" +
		"---------------------------------\nActions:\n%s\n"

 user = "\t@%s"
 gameEnded = "The game is over. Winner: %s\n"
 waitingRoom = "You are in the waiting room.\nThe game will start when there will be 4 user. %s"
 noUsers = `There are no user just now.`
 thereAreUsers = "There are several user waiting with you:\n%s\n"
 action = "\t* %s"
)

func (c *Client) replyUser(username string, status *pb.Status) {
	allUsers := make([]string, 0, len(status.AllUsers))
	username = strings.TrimSuffix(username, "\n")
	for _, usr := range status.AllUsers {
		usr := strings.TrimSuffix(usr, "\n")
		if usr == username {
			continue
		}
		allUsers = append(allUsers, fmt.Sprintf(user, usr))
	}
	switch status.GameStatus {
	case constants.StatusWaiting:
		users := noUsers
		if len(allUsers) > 0 {
			users = fmt.Sprintf(thereAreUsers, strings.Join(allUsers, "\n"))
		}
		fmt.Printf(waitingRoom, users)
	case constants.StatusStarted:
		fmt.Println("It's first day. You can to get acquainted with other players.")
		actions := make([]string, 0)
		actions = append(actions, fmt.Sprintf(action, constants.ActionSendMessage))

		fmt.Printf(gameResponse, status.Name, status.Role, strings.Join(allUsers, "\n"), strings.Join(status.Messages, "\n"), strings.Join(actions, "\n"))
	case constants.StatusDay, constants.StatusNight:
		if status.GameStatus == constants.StatusDay {
			fmt.Println("It's day.")
		} else {
			fmt.Println("It's night.")
		}
		actions := make([]string, 0)
		if status.Role != constants.RoleDead {
			if status.GameStatus == constants.StatusDay {
				actions = append(actions, fmt.Sprintf(action, constants.ActionKill))
				actions = append(actions, fmt.Sprintf(action, constants.ActionEndDay))
				if status.Role == constants.RoleCop && status.Mafia != "" {
					actions = append(actions, fmt.Sprintf(action, constants.ActionPublishMafia))
				}
				actions = append(actions, fmt.Sprintf(action, constants.ActionSendMessage))
			} else {
				if status.Role == constants.RoleMafia {
					actions = append(actions, fmt.Sprintf(action, constants.ActionKill))
					actions = append(actions, fmt.Sprintf(action, constants.ActionSendMessage))
				} else if status.Role == constants.RoleCop {
					actions = append(actions, fmt.Sprintf(action, constants.ActionCheckUser))
					actions = append(actions, fmt.Sprintf(action, constants.ActionSendMessage))
				}
			}
		}

		fmt.Printf(gameResponse, status.Name, status.Role, strings.Join(allUsers, "\n"), strings.Join(status.Messages, "\n"), strings.Join(actions, "\n"))
	case constants.StatusEnded:
		fmt.Printf(gameEnded, status.Mafia)
	case constants.StatusExited:
		fmt.Println("You exit the waiting room.")
	}
	fmt.Println()
	fmt.Println()
}

func (c *Client) getFromClient(commands chan string) {
	for {
		command, _ := c.reader.ReadString('\n')
		select {
		case value, ok := <- commands:
			if ok {
				commands <- value
				commands <- command
			} else {
				return
			}
		default:
			commands <- command
		}
		time.Sleep(time.Second)
	}
}
